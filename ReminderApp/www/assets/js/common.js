var serviceEndPoint = "";

function getServiceEndPoint() {
	var env = window.localStorage.getItem("environment");
	if(env === 'qa') {
		return "";
	} else {
		return "";
	}

}

function handleMessage(action, Message) {

	if(action) {
		new $.nd2Toast({ // The 'new' keyword is important, otherwise you would overwrite the current toast instance
		   message : Message, // Required
		   action : { // Optional (Defines the action button on the right)
			 title : "Pick phone", // Title of the action button
			 link : "/any/link.html", // optional (either link or fn or both must be set to define an action)
			 fn : function() { // function that will be triggered when action clicked
				console.log("Action Button clicked'");
			 },
			 color : "red" // optional color of the button (default: 'lime')
		   },
		   ttl : 4000 // optional, time-to-live in ms (default: 3000)
		 });
	} else {
		new $.nd2Toast({ // The 'new' keyword is important, otherwise you would overwrite the current toast instance
		   message : Message, // Required
		   ttl : 4000 // optional, time-to-live in ms (default: 3000)
		 });
	}
}

$(function() {

	$( document ).on('change','#flip2b', function () {
		var env = this.value;
		console.log("Environment=" + env);
		window.localStorage.setItem("environment", env);
	});


});
