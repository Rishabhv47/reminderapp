function showTodos() {

    var db = taskDB.OfflineDB.getInstance();
    db.allDocs({include_docs: true, descending: true}, function(err, doc) {
                console.log(err)
                 // console.log(doc.rows)
                     var  str="<div class='container'><div class='row' >"
                  for(let i=0;i<doc.rows.length;i++){
                         if(doc.rows[i].doc.mytask===undefined){
                           continue;
                         }
                      str+=`

                      <div class="col-4 " style="margin:4px;margin-left:15%;">
                      <div class="nd2-card ">

                          <div class="card-title has-supporting-text">
                          <h3 class="card-primary-title">${doc.rows[i].doc.mytask}</h3>
                          <h5 class="card-subtitle">Type: ${doc.rows[i].doc.task}</h5>
                          </div>

                       <div class="card-supporting-text has-action has-title">
                           <h4>TaskDate: ${doc.rows[i].doc.mydate}</h4>
                           <h4>Repeat: ${doc.rows[i].doc.repeat}</h4>
                           <h4>Alarm: ${doc.rows[i].doc.alarm}</h4>
                           <h4>${doc.rows[i].doc.arrDep} : ${doc.rows[i].doc.place}</h4>
                           <h4>Url: ${doc.rows[i].doc.url}</h4>


                        </div>

                      <div class="card-action">
                         <div class="row between-xs">
                          <div class="col-xs-12">
                           <div class="box">
                                <a class="ui-btn ui-btn-inline" onclick="savit(${doc.rows[i].doc._id})">EDIT</a>
                                <a class="ui-btn ui-btn-inline" onclick="deleteIt(${doc.rows[i].doc._id})">DELETE</a>
                            </div>
                          </div>
                         </div>
                      </div>
                      </div>
                      </div>

                      `
                  }



                $("#main").html(str+"</div></div>")





    });
  }


    function deleteIt(id){
      // var db = new PouchDB('todos',{ skip_setup: true });
      var db = taskDB.OfflineDB.getInstance();
      db.get(""+id).then(function (doc) {
           db.remove(doc._id, doc._rev);
           showTodos()
           handleMessage(null, "Successfully deleted");


      }).catch(function (err) {
        console.log(err);
      });


    }


   function savit(id){

                 localStorage.setItem("id",id);
                 setTimeout(()=>{
                  window.location.href="edit.html"
                 },1000)
   }



  showTodos()