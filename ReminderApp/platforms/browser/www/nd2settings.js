
  var baseurl = "http://ec2-13-233-153-126.ap-south-1.compute.amazonaws.com:8081/mappundit";
  //var baseurl = "http://localhost:8080/mappundit";

$(function() {

  // Initialize nativeDroid2

  $.nd2({
    stats : {
      analyticsUA: null // Your UA-Code for Example: 'UA-123456-78'
    },
    advertising : {
      active : false, // true | false
      path : null, // Define where the Ad-Templates are: For example: "/examples/fragments/adsense/"
      extension : null // Define the Ad-Template content Extension (Most case: ".html" or ".php")
    }
  });

	// Wait for device API libraries to load
	//
	document.addEventListener("deviceready", onDeviceReady, false);

	// device APIs are available
	//
	function onDeviceReady() {
		//document.addEventListener("pause", onPause, false);
		//document.addEventListener("resume", onResume, false);
		//document.addEventListener("menubutton", onMenuKeyDown, false);
		//document.addEventListener("backbutton", onBackButtonPressed, false);
		// Add similar listeners for other events
		//console.log('Device Ready');
	}

	function onPause() {
		// Handle the pause event
	}

	function onResume() {
		// Handle the resume event
	}

	function onMenuKeyDown() {
		// Handle the menubutton event
	}

	function onBackButtonPressed() {
		//console.log('Show Popup');
		//$('#popupDialog').popup('open');
	}
});
