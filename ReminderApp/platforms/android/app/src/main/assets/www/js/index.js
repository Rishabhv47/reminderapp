
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
		//window.plugins.tts.startup(startupWin, fail);
		var environment = window.localStorage.getItem("environment");
		var el = document.getElementById('flip2b');
		//console.log("el====>" + el);
		//console.log("environment ===============>" + environment);
		$("#flip2b").val(environment).flipswitch("refresh");
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        /*var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');*/

        console.log('Received Event: ' + id);
		//var statusElement = document.getElementById('devicestatus');
		//statusElement.innerHTML = "Device Connected";
		//window.plugins.tts.startup(startupWin, fail);
		//TTS.speak("The text to speech service is ready");
    }
};

function startupWin(result) {
 // When result is equal to STARTED we are ready to play
 if (result == TTS.STARTED) {
	window.plugins.tts.speak("The text to speech service is ready");
 }
}