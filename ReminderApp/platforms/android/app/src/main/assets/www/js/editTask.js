
// Starting


//for list in menu
let temp={'<>':'select','id':'lst'}
$("#menu").html(json2html.transform(null,temp));

//Data in the list
let data = [
  {task:"Medicine"},
  {task: "Water"},
  {task:"Meeting"},
  {task:"Grocery"},
  {task:"Others"}
           ];
let template = {'<>':'option','text':'${task}','id':'${task}'};

$('#lst').html(json2html.transform(data,template) )

//variables Declare
var task;
var repeat;
var alarm;
var longitude;
var latitude;
var loc;
var arrDep;

var rev;
function fill(){
        var db = taskDB.OfflineDB.getInstance();
        db.get(""+localStorage.getItem("id")).then(function (doc) {
                     $("#mytask").val(doc.mytask)
                     $("#mydate").val(doc.mydate),
                     $('#myurl').val(doc.url),
                     $("#flip2c").val(doc.arrDep),
                     $('#autocomplete').val(doc.place),
                     arrDep=doc.arrDep,
                     latitude=doc.latitude,
                     longitude=doc.longitude,
                     loc=doc.place,
                     repeat=doc.repeat
                     alarm=doc.alarm
                     task=doc.task
                     rev=doc._rev
                    }
                     ).catch(function (err) {
                      console.log(err);
        });
}



$(document).ready(function(){
    //for radio Button
    $("input[type='radio']").click(function(){
      var radioValue = $("input[name='radio-choice-1']:checked").val();
      if(radioValue){
         setRepeat(radioValue)
      }
  });


  //for checkbox
  $("input[type='checkbox']").click(function(){
    var radioValue = $("input[name='checkbox-1a']:checked").val();
    if(radioValue){
      setAlarm(radioValue)
    }
  });

  //for checkbox
  $("input[type='checkbox']").click(function(){
  var radioValue = $("input[name='checkbox-4a']:checked").val();
  if(radioValue){
    setAlarm(radioValue)
  }
  });


  $('#editTask').on('click',function(){
    edit()
  })



});

function setRepeat(e){
      repeat=e
}


function setAlarm(e){
     alarm=e
}

var placeSearch, autocomplete;

function initAutocomplete() {
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('autocomplete'), {types: ['geocode']});



  autocomplete.setFields(['address_component', 'geometry']);

  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {

  var place = autocomplete.getPlace();
  //console.log(place.geometry.location.lng())
    longitude=place.geometry.location.lng();
    latitude=place.geometry.location.lat();
    loc=place.address_components[0].long_name

}



$(function() {

	$( document ).on('change','#flip2c', function () {
		var flipped = this.value;
    console.log("flipped=" + flipped);
    arrDep=flipped;
	//	window.localStorage.setItem("envi", env);
	});


});




$('#lst').on('click',function(){
   //console.log($('#lst').val())
   task=$('#lst').val()
})



function edit(){
     task=task;
     url=$('#myurl').val()

    var db = taskDB.OfflineDB.getInstance();
    var todo = {
        _id:localStorage.getItem("id"),
        mytask:$("#mytask").val(),
        mydate:$("#mydate").val(),
        repeat:repeat,
        longitude:longitude,
        latitude:latitude,
        place:loc,
        arrDep:arrDep,
        url:url,
        alarm:alarm,
        task:task,
        _rev:""+rev
    };


      db.put(todo, function(err, result) {
        console.log(err)
      if (!err) {
        handleMessage(null, "Successfully edited");

             setTimeout(()=>{
              window.location.href="mytasks.html"
             },1500)
      }
    });

      console.log("hello")



}





fill()