var taskDB = taskDB || {};
taskDB.OfflineDB = (function () {
    var instance;
	var db;

    function init() {
        var instanceName = "task.db";
        //var offlineDB;

		try {
			offlineDB = new PouchDB(instanceName, {adapter: 'cordova-sqlite'});
			if (!offlineDB.adapter) {
			  // websql/sqlite not supported (e.g. FirefoxOS)
			  offlineDB = new PouchDB('task.db');
			}
		} catch(e){}
		this.db = offlineDB;
		return offlineDB;
    };

	function getTable(tableName, callback) {
		this.db.get(tableName).then(function (doc) {
		  callback(doc);
		}).catch(function (err) {
		  console.log(err);
		  callback(null);
		});
	};

	function setTable(doc, callback) {
		this.db.put(doc).then(function (response) {
		  callback(response, "Success");
		}).catch(function (err) {
		  console.log(err);
		  callback(null, "Error");
		});
	};

    return {
        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        },
		getTable: function(x,y) {
			return getTable(x,y);
		},
		setTable: function(x,y) {
			return setTable(x,y);
		}
    };
}());