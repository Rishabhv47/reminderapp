
// Starting


//for list in menu
let temp={'<>':'select','id':'lst'}
$("#menu").html(json2html.transform(null,temp));

//Data in the list
let data = [
  {task:"Medicine"},
  {task: "Water"},
  {task:"Meeting"},
  {task:"Grocery"},
  {task:"Others"}
           ];
let template = {'<>':'option','text':'${task}','id':'${task}'};

$('#lst').html(json2html.transform(data,template) )

//variables Declare
var task;
var repeat;
var alarm;
var longitude;
var latitude;
var loc;
var arrDep=$("#flip2c").val()


//Jquery on ready
$(document).ready(function(){
           //for radio Button
        $("input[type='radio']").click(function(){
            var radioValue = $("input[name='radio-choice-1']:checked").val();
            if(radioValue){
               setRepeat(radioValue)
            }
        });


        //for checkbox
        $("input[type='checkbox']").click(function(){
          var radioValue = $("input[name='checkbox-1a']:checked").val();
          if(radioValue){
            setAlarm(radioValue)
          }
        });

        //for checkbox
        $("input[type='checkbox']").click(function(){
        var radioValue = $("input[name='checkbox-4a']:checked").val();
        if(radioValue){
          setAlarm(radioValue)
        }
        });


    //for Adding the task
    $('#addTask').on('click',function(){
      create()
    })


});


//function Set reapet
function setRepeat(e){
         console.log(e)
          repeat=e
}


//function setAlarm
function setAlarm(e){
         alarm=e
}



//for autocomplete
var placeSearch, autocomplete;
function initAutocomplete() {
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('autocomplete'), {types: ['geocode']});

  autocomplete.setFields(['address_component', 'geometry']);

  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {

  var place = autocomplete.getPlace();

    longitude=place.geometry.location.lng();
    latitude=place.geometry.location.lat();
    loc=place.address_components[0].long_name


}




//for flipButton
$(function() {

	$( document ).on('change','#flip2c', function () {
		var flipped = this.value;
    console.log("flipped=" + flipped);
    arrDep=flipped;

	});


});




//for creating the task
function create(){

  task=$('#lst').val()
  url=$('#myurl').val()
  taskid=(new Date).getTime();
  var db = taskDB.OfflineDB.getInstance();
  var remoteCouch = false;
  var todo = {
      _id:""+ (new Date).getTime(),
      mytask:$("#mytask").val(),
      mydate:$("#mydate").val(),
      repeat:repeat,
      longitude:longitude,
      latitude:latitude,
      place:loc,
      arrDep:arrDep,
      url:url,
      alarm:alarm,
      task:task,
      completed: false
    };

    db.put(todo, function(err, result) {

      if (!err) {

        handleMessage(null, "Successfully created a reminder");
           setTimeout(()=>{
            window.location.href="index.html"
           },1500)

      }

    });



}
